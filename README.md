# Strapi-Gatsby-Catalog_v01d


### Purpose:

Explore feasibility of: 
* ...a Strapi CMS back-end deployed onto a "fer reelz" cloud platform (PaaS) such as Google Cloud Platform (GCP);
* ...a Multimedia Asset Management (MAM) and high-performance CDN back-end such as Cloudinary; 
* ...and a Gatsby front-end deployed onto a "high traffic" CDN such as GitLab pages, or even GCP (again).

### Caveats:

These Strapi deployments turn out not to be as straight-forward as they might initially seem.  

If you intend to put stuff into production, you're likely going to make at least two (and _maybe_ three) deployements: 
* ...one for the headless CMS; 
* ...(maybe) one for your MAM service;
* ...and one for the CDN delivery of your front-end consuming your CMS API services; 
  *  (crossed against the number of your consuming services)
* and these set-pairs crossed against your development, integration, testing, staging, and production instances. 

### History:

In fact, this is our _fourth_ attempt at this, because:

1. The first time through, we mucked up the first attempted merge of dir/file trees (especially the "hidden" `.git` dirs) between Strapi's GutHub-based project repo and our GitLab-based repo).   
2. The second time through, our Strapi build choked (deep in the bowels of `Node.js` library code) when: 
   1. We tried to use symbolic links (e.g. `./backend/config/database.js`) pointing to deployment-specific files, such as:
      * `database.sqlite.localhost.js`
      * `database.postgres.gcp.js`   
    ...go figure.
   2. To  get around this, we switched to use `git branch`-based versions of these configuration files.
3. The third time through, we misinterpreted the documentation (or it wasn't precise enough, you take your pick), and:
   * ...the natural additional complexity of a "double app" (Strapi + Gatsby) directory structure, 
   * ...a mixture of operations in implicit -- but incorrect (at least as we read them) -- directories, 
   * ...as well as a project-specific approach based on `yarn` (as Ionic/Angular developers, we're used to `npm`)   
  ...was enough to do us in.

Hopefully, this fourth time's the charm.  

### Results:

And... it worked (_see "Deployments", below_).  Yay, us!  But [it was a flippin' struggle](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/strapi-cms-ecosystem/strapi-gatsby-catalog_v01d/-/issues?scope=all&state=all) to get all the pieces configured correctly.  

Based on what we learned in this series of configuration attempts, debugging, guessing, and trial/error, we will likely re-do this project into another incarnation --  one that's more "streamlined" and ready for re-use.  We should keep this project around, as a repository of broken ideas and dreams. Yeah, I'm dark.  Like... emo-dark.  

### Approach:
* As we proceed, we'll [track our progress on a per-issue basis](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/strapi-cms-ecosystem/strapi-gatsby-catalog_v01d/-/issues), in a "GSD using micro-tasks" manner.   
* Useful SOPs can be constructed from the project history tracked via the **[closed issues](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/strapi-cms-ecosystem/strapi-gatsby-catalog_v01d/-/issues?scope=all&state=closed)**.


### Background & Resources

The Strapi site offers a [Getting Started with Gatsby](https://strapi.io/documentation/developer-docs/latest/developer-resources/content-api/integrations/gatsby.html#create-a-gatsby-app) page, with links to various starter templates.  We will follow the [Gatsby catalog starter](https://strapi.io/documentation/developer-docs/latest/developer-resources/content-api/integrations/gatsby.html#starters) link, which leads us to a GitHub project: **[Strapi Starter Gatsby Catalog](https://github.com/strapi/strapi-starter-gatsby-catalog#strapi-starter-gatsby-catalog)** with the starting structure for the catalog.

We will leverage the work of previous projects (such as: [strapi_deploy-to-gcp_v01b](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/strapi-cms-ecosystem/strapi_deploy-to-gcp_v01b)) that experiment with prototype deployments onto GCP, etc., and follow: 
* the Strapi (general) [Deployment Guide](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/deployment.html) 
* and (specific) [Google App Engine Guide](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/deployment/hosting-guides/google-app-engine.html) 
* and (reference) Strapi [Configurations Guide](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html)   

...to create a Strapi CMS instance running on GCP.

---


### Currently Deployments:
* **Localhost Instances:**
  * Using branch:  `main`
  * Strapi CMS server (on localhost): http://localhost:1337/admin/   
    * Strapi REST API Example Endpoints:
      * http://localhost:1337/products/steinway-grand-c-227  
  * Gatsby Static Site (on localhost): http://localhost:8000/
* **GCP-Deployed Instances:**
  * Using branch: `deploy-mc-ecosys-dev-v03`
  * Strapi CMS server (on GCP): https://mc-strapi-catalog-v1-dot-mc-ai-rest-service-v03.wm.r.appspot.com   
    * Strapi REST API Example Endpoints:
      * https://mc-strapi-catalog-v1-dot-mc-ai-rest-service-v03.wm.r.appspot.com/products


### PaaS Management Dashboards:
* Cloudinary Media Management PaaS: (via a [personal account](https://cloudinary.com/console/c-cb82244776fa22dd910eb8f1488974/media_library/folders/home))
* GCP Project Dashboard: [mc-ai-rest-service-v03](https://console.cloud.google.com/home/dashboard?authuser=3&project=mc-ai-rest-service-v03&folder=)
  * Note that we have already created (in previous projects):
    * 1 new database: `strapi_catalog_v1`
    * 1 new user: `strapi`


### Dynalist Action-Items & Knowledge Bases:
* Strapi Knowledge Base ([via DynaList](https://dynalist.io/d/XoMkxmR1c7xCcPHNG4zUrWEi#z=7ujn_CDYtGZ-xMse1r3VqIKe))
* Gatsby Knowledge Base ([via DynaList](https://dynalist.io/d/XoMkxmR1c7xCcPHNG4zUrWEi#z=u-B_PD2Nlr0KahscXewpfW5E))
* `git` Knowledge Base ([via DynaList](https://dynalist.io/d/XoMkxmR1c7xCcPHNG4zUrWEi#z=NvCDWmSatn8N1dcJWouIOrIR))

